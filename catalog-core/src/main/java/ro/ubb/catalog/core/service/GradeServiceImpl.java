package ro.ubb.catalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.Grade;
import ro.ubb.catalog.core.repository.GradeRepository;

import java.util.List;

@Service
public class GradeServiceImpl implements GradeService {
    public static final Logger log = LoggerFactory.getLogger(ProblemServiceImpl.class);

    @Autowired
    private GradeRepository gradeRepository;

    @Override
    public List<Grade> getAllGrades() {
        log.trace("getAllGrades --- method entered");

        List<Grade> result = gradeRepository.findAll();

        log.trace("getAllGrades: result={}", result);

        return result;
    }

    @Override
    public Grade saveGrade(Grade grade) {
        //todo: log
        return gradeRepository.save(grade);
    }

    @Override
    @Transactional
    public Grade updateGrade(Long id, Grade grade) {
        //todo log

        Grade update = gradeRepository.findById(id).orElse(grade);
        update.setGrade(grade.getGrade());
        update.setProblemID(grade.getProblemID());
        update.setStudentID(grade.getStudentID());

        return update;
    }

    @Override
    public void deleteGrade(Long id) {
//todo log
        gradeRepository.deleteById(id);
    }
}