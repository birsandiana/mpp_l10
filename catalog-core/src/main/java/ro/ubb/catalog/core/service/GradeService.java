package ro.ubb.catalog.core.service;

import ro.ubb.catalog.core.model.Grade;


import java.util.List;

public interface GradeService {
    List<Grade> getAllGrades();

    Grade saveGrade(Grade grade);

    Grade updateGrade(Long id, Grade grade);

    void deleteGrade(Long id);
}
