package ro.ubb.catalog.core.validator;

import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.Student;

import java.util.Optional;

@Component
public class StudentValidator implements Validator<Student> {
    @Override
    public void validate(Student entity) throws ValidatorException {
        Optional.ofNullable(entity.getGroupnumber() == (int) (entity.getGroupnumber())).orElseThrow(() -> new ValidatorException("Group must be integer"));
        Optional.ofNullable(entity.getGroupnumber() <= 1000 && entity.getGroupnumber() >= 0).orElseThrow(() -> new ValidatorException("Group must be between 0 and 1000"));
        Optional.ofNullable(entity.getName() instanceof String).orElseThrow(() -> new ValidatorException("Name must be string"));
    }
}