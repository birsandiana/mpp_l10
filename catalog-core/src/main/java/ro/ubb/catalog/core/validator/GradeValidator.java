package ro.ubb.catalog.core.validator;


import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.Grade;

import java.util.Optional;

@Component
public class GradeValidator implements Validator<Grade>{
    @Override
    public void validate(Grade entity) throws ValidatorException {
        Optional.ofNullable(entity.getProblemID() instanceof Long).orElseThrow(() -> new ValidatorException("Problem must be long"));
        Optional.ofNullable(entity.getStudentID() instanceof Long).orElseThrow(() -> new ValidatorException("Problem must be long"));
        Optional.ofNullable(entity.getGrade() == (int)(entity.getGrade())).orElseThrow(() -> new ValidatorException("Grade must be int"));
    }
}
