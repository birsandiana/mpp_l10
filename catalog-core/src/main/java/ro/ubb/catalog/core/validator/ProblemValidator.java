package ro.ubb.catalog.core.validator;

import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.Problem;

import java.util.Optional;

@Component
public class ProblemValidator implements Validator<Problem> {
    @Override
    public void validate(Problem entity) throws ValidatorException {
        Optional.ofNullable(entity.getText() instanceof String).orElseThrow(()->new ValidatorException("Text must be string"));

    }
}