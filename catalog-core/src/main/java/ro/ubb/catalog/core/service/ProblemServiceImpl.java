package ro.ubb.catalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.Problem;
import ro.ubb.catalog.core.model.Student;
import ro.ubb.catalog.core.repository.ProblemRepository;
import ro.ubb.catalog.core.repository.StudentRepository;

import java.util.List;

@Service
public class ProblemServiceImpl implements ProblemService {
    public static final Logger log = LoggerFactory.getLogger(ProblemServiceImpl.class);

    @Autowired
    private ProblemRepository problemRepository;

    @Override
    public List<Problem> getAllProblems() {
        log.trace("getAllProblems --- method entered");

        List<Problem> result = problemRepository.findAll();

        log.trace("getAllProblems: result={}", result);

        return result;
    }

    @Override
    public Problem saveProblem(Problem problem) {
        //todo: log
        return problemRepository.save(problem);
    }

    @Override
    @Transactional
    public Problem updateProblem(Long id, Problem problem) {
        //todo log

        Problem update = problemRepository.findById(id).orElse(problem);
        update.setText(problem.getText());

        return update;
    }

    @Override
    public void deleteProblem(Long id) {
//todo log
        problemRepository.deleteById(id);
    }
}
