package ro.ubb.catalog.core.validator;

public interface Validator<T>{
    void validate(T entity) throws ValidatorException;
}
