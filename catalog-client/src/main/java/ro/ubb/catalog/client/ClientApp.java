package ro.ubb.catalog.client;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.client.RestTemplate;
import ro.ubb.catalog.core.validator.ValidatorException;
import ro.ubb.catalog.web.dto.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * Created by radu.
 */
public class ClientApp {
    private static String urlStudents = "http://localhost:8080/api/students";
    private static String urlProblems = "http://localhost:8080/api/problems";
    private static String urlGrades = "http://localhost:8080/api/grades";

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(
                        "ro.ubb.catalog.client.config"
                );

        RestTemplate restTemplate = context.getBean(RestTemplate.class);

//        StudentDto savedStudent = restTemplate.postForObject(
//                urlStudents,
//                new StudentDto("sn1", "s1", 111),
//                StudentDto.class);
//        System.out.println("savedStudent: " + savedStudent);

        int input = 1;
        Scanner in = new Scanner(System.in);
        menu();
        while (input != 0) {
            System.out.println("Give input: ");
            input = in.nextInt();
            System.out.println(input);
            if (input == 1)
                addStudents(restTemplate);
            else if (input == 2)
                addProblems(restTemplate);
            else if (input == 3)
                addGrades(restTemplate);
            else if (input == 4)
                printAllStudents(restTemplate);
            else if (input == 5)
                printAllProblems(restTemplate);
            else if (input == 6)
                printAllGrades(restTemplate);
            else if (input == 7)
                deleteStudent(restTemplate);
            else if (input == 8)
                deleteProblem(restTemplate);
            else if (input == 9)
                deleteGrade(restTemplate);
            else if (input == 10)
                updateStudent(restTemplate);
            else if (input == 11)
                updateProblem(restTemplate);
            else if (input == 12)
                updateGrade(restTemplate);
            else if (input != 0)
                System.out.println("Incorrect input");
        }



        System.out.println("bye ");
    }

    private static void printAllStudents(RestTemplate restTemplate) {
        StudentsDto allStudents = restTemplate.getForObject(urlStudents, StudentsDto.class);
        System.out.println(allStudents);
    }

    private static void printAllProblems(RestTemplate restTemplate) {
        ProblemsDto allProblems = restTemplate.getForObject(urlProblems, ProblemsDto.class);
        System.out.println(allProblems);
    }

    private static void printAllGrades(RestTemplate restTemplate) {
        GradesDto allGrades = restTemplate.getForObject(urlGrades, GradesDto.class);
        System.out.println(allGrades);
    }

    private static GradeDto readGrade() throws ValidatorException {
        System.out.println("Read grade {studID, probID, grade}");

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            Long sid = Long.valueOf(bufferRead.readLine());
            Long pid = Long.valueOf(bufferRead.readLine());
            int g = Integer.parseInt(bufferRead.readLine());

//            if(!studentService.findOne(sid).isPresent()){
//                throw new ValidatorException("Student ID must be an existing one!");
//            }
//
//            if(!problemService.findOne(pid).isPresent()){
//                throw new ValidatorException("Problem ID must be an existing one!");
//            }

            return new GradeDto(sid, pid, g);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private static ProblemDto readProblem() {
        System.out.println("Read problem {text}");

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            String text = bufferRead.readLine();

            return new ProblemDto(text);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private static StudentDto readStudent() {
        System.out.println("Read student {serialNumber, name, group}");

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            String serialNumber = bufferRead.readLine();
            String name = bufferRead.readLine();
            int group = Integer.parseInt(bufferRead.readLine());

            return new StudentDto(serialNumber, name, group);
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (NumberFormatException e) {
            System.out.println("Group can't be a string!");
        }
        return null;
    }

    private static void addStudents(RestTemplate restTemplate) {
        StudentDto student = readStudent();
        StudentDto savedStudent = restTemplate.postForObject(
                urlStudents,
                student,
                StudentDto.class);
        System.out.println("savedStudent: " + savedStudent);
    }

    private static void addProblems(RestTemplate restTemplate) {
        ProblemDto problem = readProblem();
        ProblemDto savedProblem = restTemplate.postForObject(
                urlProblems,
                problem,
                ProblemDto.class);
        System.out.println("savedProblem: " + savedProblem);
    }

    private static void addGrades(RestTemplate restTemplate) {
        GradeDto grade = readGrade();
        GradeDto savedGrade = restTemplate.postForObject(
                urlGrades,
                grade,
                GradeDto.class);
        System.out.println("savedGrade: " + savedGrade);
    }

    private static void deleteGrade(RestTemplate restTemplate) {
        System.out.println("Introduce the ID of the grade you wanna delete");
        Scanner in = new Scanner(System.in);
        Long id = in.nextLong();

        restTemplate.delete(urlGrades + "/{id}", id);
    }

    private static void deleteProblem(RestTemplate restTemplate) {
        System.out.println("Introduce the ID of the problem you wanna delete");
        Scanner in = new Scanner(System.in);
        Long id = in.nextLong();

        restTemplate.delete(urlProblems + "/{id}", id);
    }

    private static void deleteStudent(RestTemplate restTemplate) {
        System.out.println("Introduce the ID of the student you wanna delete");
        Scanner in = new Scanner(System.in);
        Integer id = in.nextInt();

        restTemplate.delete(urlStudents + "/{id}", id);
    }

    private static void updateGrade(RestTemplate restTemplate) {
        System.out.println("Introduce the ID of the grade you wanna update");
        Scanner in = new Scanner(System.in);
        Long id = in.nextLong();
        GradeDto grade2 = readGrade();
        grade2.setId(id);
        restTemplate.put(urlGrades + "/{id}", grade2, grade2.getId());
    }

    private static void updateProblem(RestTemplate restTemplate) {
        System.out.println("Introduce the ID of the problem you wanna update");
        Scanner in = new Scanner(System.in);
        Long id = in.nextLong();
        ProblemDto prob2 = readProblem();
        prob2.setId(id);
        restTemplate.put(urlProblems + "/{id}", prob2, prob2.getId());
    }

    private static void updateStudent(RestTemplate restTemplate) {
        System.out.println("Introduce the ID of the problem you wanna update");
        Scanner in = new Scanner(System.in);
        Long id = in.nextLong();
        StudentDto student2 = readStudent();
        student2.setId(id);
        restTemplate.put(urlStudents + "/{id}", student2, student2.getId());
    }


    private static void menu() {
        System.out.println("========== MENU ==========");
        System.out.println("1. Add student");
        System.out.println("2. Add problem");
        System.out.println("3. Add grade");
        System.out.println("4. All students");
        System.out.println("5. All problems");
        System.out.println("6. All grades");
        System.out.println("7. Delete student");
        System.out.println("8. Delete problem");
        System.out.println("9. Delete grade");
        System.out.println("10. Update student");
        System.out.println("11. Update problem");
        System.out.println("12. Update grade");
        System.out.println("0. Exit");
    }

}