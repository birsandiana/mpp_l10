package ro.ubb.catalog.web.converter;

import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.Grade;

import ro.ubb.catalog.web.dto.GradeDto;

@Component
public class GradeConverter extends BaseConverter<Grade, GradeDto> {
    @Override
    public Grade convertDtoToModel(GradeDto dto) {
        Grade problem = Grade.builder()
                .grade(dto.getGrade())
                .studentID(dto.getStudentID())
                .problemID(dto.getProblemID())
                .build();
        problem.setId(dto.getId());
        return problem;
    }

    @Override
    public GradeDto convertModelToDto(Grade grade) {
        GradeDto dto = GradeDto.builder()
                .grade(grade.getGrade())
                .studentID(grade.getStudentID())
                .problemID(grade.getProblemID())
                .build();
        dto.setId(grade.getId());
        return dto;
    }
}
