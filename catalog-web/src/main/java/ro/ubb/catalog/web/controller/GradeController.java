package ro.ubb.catalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.service.GradeService;
import ro.ubb.catalog.web.converter.GradeConverter;
import ro.ubb.catalog.web.dto.*;

@RestController
public class GradeController {
    public static final Logger log= LoggerFactory.getLogger(GradeController.class);

    @Autowired
    private GradeService gradeService;

    @Autowired
    private GradeConverter gradeConverter;


    @RequestMapping(value = "/grades", method = RequestMethod.GET)
    GradesDto getGrades() {
        //todo: log
        return new GradesDto(gradeConverter
                .convertModelsToDtos(gradeService.getAllGrades()));

    }

    @RequestMapping(value = "/grades", method = RequestMethod.POST)
    GradeDto saveGrade(@RequestBody GradeDto gradeDto) {
        //todo log
        return gradeConverter.convertModelToDto(gradeService.saveGrade(
                gradeConverter.convertDtoToModel(gradeDto)
        ));
    }

    @RequestMapping(value = "/grades/{id}", method = RequestMethod.PUT)
    GradeDto updateGrade(@PathVariable Long id,
                             @RequestBody GradeDto gradeDto) {
        //todo: log
        return gradeConverter.convertModelToDto( gradeService.updateGrade(id,
                gradeConverter.convertDtoToModel(gradeDto)));
    }

    @RequestMapping(value = "/grades/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteGrade(@PathVariable Long id){
        //todo:log

        gradeService.deleteGrade(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
