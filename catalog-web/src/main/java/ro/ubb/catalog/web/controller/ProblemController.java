package ro.ubb.catalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.service.ProblemService;
import ro.ubb.catalog.core.service.StudentService;
import ro.ubb.catalog.web.converter.ProblemConverter;
import ro.ubb.catalog.web.converter.StudentConverter;
import ro.ubb.catalog.web.dto.ProblemDto;
import ro.ubb.catalog.web.dto.ProblemsDto;
import ro.ubb.catalog.web.dto.StudentDto;
import ro.ubb.catalog.web.dto.StudentsDto;

@RestController
public class ProblemController {
    public static final Logger log= LoggerFactory.getLogger(ProblemController.class);

    @Autowired
    private ProblemService problemService;

    @Autowired
    private ProblemConverter problemConverter;


    @RequestMapping(value = "/problems", method = RequestMethod.GET)
    ProblemsDto getProblems() {
        //todo: log
        return new ProblemsDto(problemConverter
                .convertModelsToDtos(problemService.getAllProblems()));

    }

    @RequestMapping(value = "/problems", method = RequestMethod.POST)
    ProblemDto saveProblem(@RequestBody ProblemDto problemDto) {
        //todo log
        return problemConverter.convertModelToDto(problemService.saveProblem(
                problemConverter.convertDtoToModel(problemDto)
        ));
    }

    @RequestMapping(value = "/problems/{id}", method = RequestMethod.PUT)
    ProblemDto updateProblem(@PathVariable Long id,
                             @RequestBody ProblemDto problemDto) {
        //todo: log
        return problemConverter.convertModelToDto( problemService.updateProblem(id,
                problemConverter.convertDtoToModel(problemDto)));
    }

    @RequestMapping(value = "/problems/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteProblem(@PathVariable Long id){
        //todo:log

        problemService.deleteProblem(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
